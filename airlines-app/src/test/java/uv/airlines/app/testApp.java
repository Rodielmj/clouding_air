package uv.airlines.app;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;



/**
 * testApp
 */
public class testApp {

    public static void main(String[] args) {

        //  Date date = new Date(new Long(1559260800L * 1000));
         Date date = new Date();
        // LocalDateTime dateTime = LocalDateTime.fromDateFields(date);
        Calendar c = Calendar.getInstance();
        System.out.println(" Hoy: " + c.getTime());

        c.add(Calendar.MONTH, 1);

        System.out.println("Antes: " + c.getTime());
    }


    public void generateAutomatic(){
        List<String> seatBusy = new ArrayList<>();
        Integer planeCapacity = 30;
        HashMap<String, ArrayList<Integer>> test = new HashMap<>();
        ArrayList<Integer> seatNumber = new ArrayList<>();
        Integer passengerWithoutSeat = 10;

        seatBusy.add("A2");
        seatBusy.add("B5");
        seatBusy.add("B8");

        for (int i = 1; i <= 10; i++) {
            seatNumber.add(i);
        }

        test.put("A", seatNumber);
        test.put("B", seatNumber);
        test.put("C", seatNumber);
        Integer seatFreeCount = 0;

        for (String seat : seatBusy) {
            String letter = seat.substring(0, 1);
            String number = seat.substring(1);
            ArrayList<Integer> seatFree = (ArrayList<Integer>) test.get(letter).stream()
                    .filter(s -> s != Integer.parseInt(number)).collect(Collectors.toList());

            test.put(letter, seatFree);
            seatFreeCount = seatFreeCount + seatFree.size();
            if (seatFreeCount > passengerWithoutSeat) {
                break;
            }
         }

        test.forEach((k, v) -> {
            v.stream().forEach(i -> {
                System.out.println("");
            });
        });
    }

}